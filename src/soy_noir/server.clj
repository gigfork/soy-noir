(ns soy-noir.server
  (:require [noir.server :as server]))

(server/load-views-ns 'soy-noir.views)

(defn -main [& m]
  (let [mode (keyword (or (first m) :dev))
        port 9090]
    (server/start port {:mode mode
                        :ns 'soy-noir})))

